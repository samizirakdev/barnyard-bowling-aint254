﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadLevels : MonoBehaviour {

	// Use this for initialization
    public void LoadControls() // Loads the controls screen when called
    {
        SceneManager.LoadScene(1);
    }
    
	public void LoadTestLevel() // Loads the tutorial level when called when called
    {
        SceneManager.LoadScene(2);
	}

    public void LoadMenu() // Loads the main menu when called
    {
        SceneManager.LoadScene(0);
    }
    public void ExitApplication() // Quits the game when called
    {
        Application.Quit();
    }

    public void LoadLvlTwo() // Loads the second level when called
    {
        SceneManager.LoadScene(3);
    }
	// Update is called once per frame
	void Update () {
	
	}
}

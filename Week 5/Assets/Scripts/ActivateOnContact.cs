﻿using UnityEngine;
using System.Collections;

public class ActivateOnContact : MonoBehaviour {

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Projectile(Clone)" || col.gameObject.name == "Projectile_two(Clone)") // If either of the two projectiles collide with the switch
        {
            Destroy(GameObject.Find("Defender Left")); //
            Destroy(GameObject.Find("Defender Right")); //
            Destroy(GameObject.Find("Static Defender")); // Destroy the three in game barricades
            Destroy(col.gameObject); // This destryos the projectile once it comes in contact with the switch
            Destroy(gameObject); // This destroys the the switch when the projectile comes in contact with it
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class ActivateOnContactLvl2 : MonoBehaviour {

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Projectile(Clone)" || col.gameObject.name == "Projectile_two(Clone)")
        {
            Destroy(GameObject.Find("Static Defender (1)"));
            Destroy(col.gameObject);
            Destroy(gameObject);
        }
    }
}

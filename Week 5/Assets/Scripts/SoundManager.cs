﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour {

    [SerializeField]
    private AudioMixer m_masterMixer;

    [SerializeField]
    private AudioMixerSnapshot m_menuMode;

    [SerializeField]
    private AudioMixerSnapshot m_gameMode;

    public void SetMasterVolume (float _volume)
    {
        m_masterMixer.SetFloat("MasterVol", _volume);
    }

    public void MenuModeOn()
    {

        m_menuMode.TransitionTo(0.5f);

    }

    public void GameModeOn()
    {

        m_gameMode.TransitionTo(0.5f);

    }
}

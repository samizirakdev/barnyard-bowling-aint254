﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShotsLeftUI : MonoBehaviour {

    public Image shotOne;
    public Image shotTwo;
    public Image shotThree;
    public Image shotFour;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {

        GameObject cannonP = GameObject.Find("Cannon Top");

        Projectile projectileScript = cannonP.GetComponent<Projectile>();

        if (projectileScript.shotsLeft == 3)
        {
            shotFour.enabled = false;
        }

        if (projectileScript.shotsLeft == 2)
        {
            shotFour.enabled = false;
            shotThree.enabled = false;
        }

        if (projectileScript.shotsLeft == 1)
        {
            shotFour.enabled = false;
            shotThree.enabled = false;
            shotTwo.enabled = false;
        }

        if (projectileScript.shotsLeft == 0)
        {
            shotFour.enabled = false;
            shotThree.enabled = false;
            shotTwo.enabled = false;
            shotOne.enabled = false;
        }
    }
}

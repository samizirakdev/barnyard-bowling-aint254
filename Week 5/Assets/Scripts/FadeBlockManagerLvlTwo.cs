﻿using UnityEngine;
using System.Collections;

public class FadeBlockManagerLvlTwo : MonoBehaviour {

    public GameObject fadeBlockOne; //
    public GameObject fadeBlockTwo; //
    public GameObject fadeBlockThree; //
    public GameObject fadeBlockFour; // Allows you assign 4 fade blocks that will appear and disapear

    public bool setTwoActive;


    // Use this for initialization
    void Start()
    {
        fadeBlockOne.SetActive(true);
        fadeBlockTwo.SetActive(false);
        fadeBlockThree.SetActive(true);
        fadeBlockFour.SetActive(false);
        setTwoActive = false;

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonUp("FadeBlockOne")) // If Left Ctrl is pressed set the bool to true
        {
            setTwoActive = true;
        }

        if (Input.GetButtonUp("FadeBlockTwo")) // If Right Alt is pressed set the bool to false
        {
            setTwoActive = false;
        }

        if (setTwoActive == false) // If the setTwoActive bool is false one pair of fade blocks is made active and the other pari are made false
        {
            fadeBlockOne.SetActive(true);
            fadeBlockTwo.SetActive(false);
            fadeBlockThree.SetActive(true);
            fadeBlockFour.SetActive(false);
        }

        if (setTwoActive == true) // If the setTwoActive bool is true  the other pair of fade blooks are made active and the other pair are made false
        {
            fadeBlockOne.SetActive(false);
            fadeBlockTwo.SetActive(true);
            fadeBlockThree.SetActive(false);
            fadeBlockFour.SetActive(true);
        }
    }
}

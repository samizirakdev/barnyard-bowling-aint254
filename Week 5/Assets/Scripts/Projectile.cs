﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Projectile : MonoBehaviour {

    [SerializeField]
    private GameObject m_dot;

    public GameObject m_animalOne;

    public GameObject m_animalTwo;

    private GameObject[] m_line;

    public Camera cameraCustom;

    public GameObject gameOverText; 

    [SerializeField]
    private float m_force = 5;

    private Vector3 m_direction = Vector3.zero;

    public int forceLvl = 4;

    public int shotsLeft = 4;

    public int selectedProjectile = 1;
  

   
    void Start ()
    {       
         m_line = new GameObject[10]; // Generates an array of dots to be used as an aiming cursor
      
        gameOverText.SetActive(false); // Sets the game over text to false so its not viewable at first

        for (int i = 0; i < 10; i++) 
        {
            GameObject tempGameObj = Instantiate(m_dot, Vector3.zero, Quaternion.identity) as GameObject;

            tempGameObj.SetActive(false);

            m_line[i] = tempGameObj;
        }

	}
	
    private void Aim(Vector3 _direction) 
    {
        float Vox = _direction.x * m_force;

        float Voy = _direction.y * m_force;

        for (int i = 0; i < m_line.Length; i++)
        {
            float t = i * 0.1f;

            m_line[i].transform.position = new Vector3(transform.position.x + Vox * t,
                transform.position.y + ((Voy * t - 0.5f * 1.0f * t * t)), transform.position.z);

            m_line[i].SetActive(true);
        }
    }

    public GameObject AnimalToFire() // Finds out which projectile the player has selected to be fired and returns that when called
    {
        GameObject m_animalToFire = null; 

        if(selectedProjectile == 1)
        {
            m_animalToFire = m_animalOne;
        }

       if (selectedProjectile == 2)
        {
            m_animalToFire = m_animalTwo;
        }

        
        return m_animalToFire;

 
    }

    private void Fire (Vector3 _path) 
    {
        if(Time.timeScale == 1)
        {
            if (shotsLeft > 0)
            {
                shotsLeft = shotsLeft - 1;

                Vector3 cannonPos = GameObject.Find("Animal Cannon").transform.position; //Finds cannon position

                GameObject bullet = Instantiate(AnimalToFire(), cannonPos, Quaternion.identity) as GameObject; //Spawns a projectile at the position of the cannon

                bullet.GetComponent<Rigidbody>().AddForce(m_direction * m_force * forceLvl, ForceMode.Impulse); //Fires the projectile with a set amount of force         

                Destroy(bullet, 5.0f); // Destroys the fired bulelt after a certain amount of time                   
            }

            for (int i = 0; i < m_line.Length; i++) //Makes the aiming points invisible again
            {
                m_line[i].SetActive(false);
            }
        }
    }

	void Update ()
    {
        if(Time.timeScale == 1)
        {
            if (Input.GetKey("1")) // Pressing key "1" changes to projectile 1
            {
                selectedProjectile = 1;
            }

            if (Input.GetKey("2")) //Pressing key "2" changes to projectile 2
            {
                selectedProjectile = 2;
            }

            if (Input.GetButtonUp("Increase Power") && forceLvl < 8) //Increases the power of the projectile before its shot if the "D" key is pressed
            {
                forceLvl = forceLvl + 1;
            }

            if (Input.GetButtonUp("Decrease Power") && forceLvl > 4) //Decreases the power of the projectile before its shot if the "A" key is pressed.
            {
                forceLvl = forceLvl - 1;
            }

            if (shotsLeft == 0 && GameObject.Find("Projectile(Clone)") == null && GameObject.Find("Projectile_two(Clone") == null && GameObject.Find("Boss") != null) // If there are no projectiles left and a enemy is still alive it takes you to the game over screen.
            {
                SceneManager.LoadScene(0);
            }
            
            if (GameObject.Find("Boss") == null)
            {
                SceneManager.LoadScene(3);
            }

            if (Input.GetMouseButton(0)) //Generates aiming points whilst left mouse button is held down
            {
                Vector3 tempPos = cameraCustom.WorldToScreenPoint(transform.position);

                tempPos.z = transform.position.z;

                m_direction = (Input.mousePosition - tempPos).normalized;

                Aim(m_direction);
            }

            if (Input.GetMouseButtonUp(0)) // Fires a single projectile when left mouse button is released
            {
                Vector3 tempFire = cameraCustom.WorldToScreenPoint(transform.position);

                tempFire.z = transform.position.z;

                Fire(tempFire);
            }
        }
    }     
}

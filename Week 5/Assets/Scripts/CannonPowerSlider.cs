﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CannonPowerSlider : MonoBehaviour {

    public Slider cannonPower;

	// Use this for initialization
	void Start ()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {

        GameObject cannonP = GameObject.Find("Cannon Top");

        Projectile projectileScript = cannonP.GetComponent<Projectile>();

        cannonPower.value = projectileScript.forceLvl;

    }
}

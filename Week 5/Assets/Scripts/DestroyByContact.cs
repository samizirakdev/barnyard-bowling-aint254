﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour
{
    //ParticleSystem enemyExplosion;

    public GameObject enemyExplosion;

    void Awake ()
    {
        //enemyExplosion = GetComponentInChildren<ParticleSystem>();
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Floor")
        {
            Destroy(gameObject);
        }

        if (col.gameObject.name == "Projectile(Clone)" || col.gameObject.name == "Projectile_two(Clone)")
        {
            Destroy(col.gameObject);
            Destroy(gameObject);

            PlayExplosion();
        }
    }
   
    void PlayExplosion()
    {
        //enemyExplosion.Play();
        Instantiate(enemyExplosion, transform.position, transform.rotation);
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextUpdate : MonoBehaviour
{
    private Text m_text;

	// Use this for initialization
	void Start ()
    {
        m_text = GetComponent<Text>();
        UpdateText(0);
    }
	
	// Update is called once per frame
	public void UpdateText(float _value)
    {
        int sliderVal =(int)( (_value + 80) * 100/ 80);

        m_text.text = sliderVal.ToString() + " %";
	}
}

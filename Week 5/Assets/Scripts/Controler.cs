﻿using UnityEngine;
using System.Collections;

public class Controler : MonoBehaviour {

    void OnGUI()
    {
        GUI.Label(new Rect(5.0f, 5.0f, 200.0f, 25.0f), "Left X-Axis: ");
        GUI.Label(new Rect(155.0f, 5.0f, 200.0f, 25.0f), Input.GetAxis("Horizontal").ToString());

        GUI.Label(new Rect(5.0f, 20.0f, 200.0f, 25.0f), "Left Y-Axis: ");
        GUI.Label(new Rect(155.0f, 20.0f, 200.0f, 25.0f), Input.GetAxis("Vertical").ToString());

        GUI.Label(new Rect(5.0f, 35.0f, 200.0f, 25.0f), "Right X-Axis: ");
        GUI.Label(new Rect(155.0f, 35.0f, 200.0f, 25.0f), Input.GetAxis("Right X-Axis").ToString());

        GUI.Label(new Rect(5.0f, 50.0f, 200.0f, 25.0f), "Right Y-Axis: ");
        GUI.Label(new Rect(155.0f, 50.0f, 200.0f, 25.0f), Input.GetAxis("Right Y-Axis").ToString());

        if (Mathf.Abs(Input.GetAxis("Horizontal")) < 0.25)
        {

        }
    }
}

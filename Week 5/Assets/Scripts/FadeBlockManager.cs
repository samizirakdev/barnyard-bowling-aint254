﻿using UnityEngine;
using System.Collections;

public class FadeBlockManager : MonoBehaviour {

    public GameObject fadeBlockOne;
    public GameObject fadeBlockTwo;

    public bool setTwoActive;


	// Use this for initialization
	void Start ()
    {
        fadeBlockOne.SetActive(true);
        fadeBlockTwo.SetActive(false);

        setTwoActive = false;
        
	}

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonUp("FadeBlockOne"))
        {
            setTwoActive = true;
        }

        if (Input.GetButtonUp("FadeBlockTwo"))
        { 
            setTwoActive = false;
        }       

	    if (setTwoActive == false)
        {
            fadeBlockOne.SetActive(true);
            fadeBlockTwo.SetActive(false);
        }

        if (setTwoActive == true)
        {
            fadeBlockOne.SetActive(false);
            fadeBlockTwo.SetActive(true);
        }
	}
}

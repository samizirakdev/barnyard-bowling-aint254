﻿using UnityEngine;
using System.Collections;

namespace ISS
{
    public class Follow : MonoBehaviour
    {

        private Transform m_transform;
        private Vector3 pointWorld;
        // Use this for initialization
        void Start()
        {
            m_transform = transform;
        }

        // Update is called once per frame
        void Update()
        {
            pointWorld = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));

            Vector3 cubePosition = m_transform.position;

            Vector3 rotate = Vector3.Normalize(cubePosition - pointWorld);

            float angle = Mathf.Atan2(rotate.y,rotate.x) * Mathf.Rad2Deg;

            m_transform.rotation = Quaternion.Euler(0.0f, angle, 0.0f);

        }

        void OnDrawGizmosSelected()
        {
            pointWorld = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));

            Gizmos.color = Color.blue;

            Gizmos.DrawSphere(pointWorld, 0.1f);

        }
    }
}
